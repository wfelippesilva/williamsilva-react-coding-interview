import { IPerson } from '@lib/models/person';
import { useCallback, useEffect, useState } from 'react';

import { contactsClient } from '@lib/clients/contacts';

const fetchContact = (id: string,) => contactsClient.getContactById(id)

const updateContact = (id: string, updated: IPerson) => contactsClient.updateContact(id, updated)

export function useContactEdit(id: string) {
  const [loading, setLoading] = useState(true);
  const [hasError, setHasError] = useState(true);
  const [contact, setContact] = useState<IPerson | null>(null)

  useEffect(() => {
    const getContact = async () => {
      try {
        setLoading(true)
        const result = await fetchContact(id)
        setContact(result)
        setHasError(false)
      } catch {
        setHasError(true)
      } finally {
        setLoading(false)
      }
    }

    getContact()
  }, [id])

  return {
    contact,
    loading,
    hasError,
    update: useCallback((updated: IPerson) => {
      updateContact(id, updated)
    }, [])
  };
}
