
import { useContactEdit } from "@hooks/contacts/useContactEdit";
import { IPerson } from "@lib/models/person";
import React from "react";
import { useParams } from "react-router-dom";

export const ContactEditPage = () => {
  const { id } = useParams<{ id: string }>();
  const { loading, hasError, contact, update } = useContactEdit(id);

  if (loading) {
    return <p>loading...</p>;
  }

  if (hasError) {
    return <p>error =(</p>;
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement & {
    firstName: HTMLInputElement,
    lastName: HTMLInputElement,
    email: HTMLInputElement
  }>) => {
    event.preventDefault()

    const updated: IPerson = {
      id,
      firstName: event.currentTarget.firstName.value,
      lastName: event.currentTarget.lastName.value,
      email: event.currentTarget.email.value,
    };

    update(updated);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="firstName">First Name</label>
        <input
          id="firstName"
          name="firstName"
          defaultValue={contact.firstName}
        />
      </div>

      <div>
        <label htmlFor="lastName">Last Name</label>
        <input id="lastName" name="lastName" defaultValue={contact.lastName} />
      </div>

      <div>
        <label htmlFor="email">Email</label>
        <input id="email" name="email" defaultValue={contact.email} />
      </div>

      <button type="submit">Update</button>
    </form>
  );
};
